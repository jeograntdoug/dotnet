﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day06CustomSandwich
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        string _bread, _veggies, _meat;
        public enum BreadEnum { White, WholeWeat, Bagel}
        public CustomDialog()
        {
            InitializeComponent();
            dlg_comboBread.ItemsSource = Enum.GetValues(typeof(BreadEnum));
            dlg_comboBread.SelectedIndex = 0;
        }

        private void dlg_btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void dlg_btSave_Click(object sender, RoutedEventArgs e)
        {
            if(dlg_comboBread.SelectedItem == null) {
                MessageBox.Show(this, "Please select Bread.", "Bread is empty");
                return;
            }
            Bread = dlg_comboBread.SelectedItem?.ToString();

            List<string> veggieList = new List<string>();
            if (dlg_cbCucumbers.IsChecked == true)
            {
                veggieList.Add("Cucumbers");
            }
            if (dlg_cbTomatoes.IsChecked == true)
            {
                veggieList.Add("Tomatoes");
            }
            if (dlg_cbLettuce.IsChecked == true)
            {
                veggieList.Add("Lettuce");
            }
            Veggies = string.Join(",", veggieList);

            string meat;
            if (dlg_rbChicken.IsChecked == true)
            {
                Meat = "Chicken";
            }else if (dlg_rbTurkey.IsChecked == true)
            {
                Meat = "Turkey";
            }else if (dlg_rbTofu.IsChecked == true)
            {
                Meat = "Tofu";
            } else
            {
                MessageBox.Show(this, "This shouldn't happen", "Internal Error");
                return;
            }

            this.DialogResult = true;
        }

        public string Bread
        {
            get { return _bread; }
            private set { _bread = value; }
        }
        public string Veggies
        {
            get{ return _veggies; }
            private set { _veggies = value; }
        }

        public string Meat
        {
            get
            {
                return _meat;
            }
            private set { _meat = value; }
        }
    }
}
