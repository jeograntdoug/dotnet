﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day06CustomSandwich
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btMakeASandwich_Click(object sender, RoutedEventArgs e)
        {
            CustomDialog customDialog = new CustomDialog();
            customDialog.Owner = this;
            if (customDialog.ShowDialog() == true)
            {
                lblBread.Content = customDialog.Bread;
                lblVeggies.Content = customDialog.Veggies;
                lblMeat.Content = customDialog.Meat;
            }
        }
    }
}
