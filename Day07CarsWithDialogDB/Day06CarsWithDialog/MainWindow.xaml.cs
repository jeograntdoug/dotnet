﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CsvHelper;
using Day06CarsWithDialog;
using Microsoft.Win32;

namespace Day07CarsWithDialogDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Car> carList = new List<Car>();
        Database db;
        public MainWindow()
        {
            InitializeComponent();

            try { 
                db = new Database();
                carList = db.GetAllCars();
                lvCars.ItemsSource = carList;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this,
                    "Database Connection Fail",
                    "Database Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }


        private void EditCar()
        {
            Car currentCar = (Car)lvCars.SelectedItem;
            if (currentCar == null)
            {
                return;
            }

            CustomDialog dlg = new CustomDialog(currentCar);

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    string statusStr = "Editted: \"" + currentCar + "\"=> ";
                    dlg.NewCar.Id = currentCar.Id;
                    db.UpdateCar(dlg.NewCar);

                    txtStatus.Text = statusStr + dlg.NewCar;
                    lvCars.ItemsSource = db.GetAllCars();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(this,
                        "Database Connection Fail",
                        "Database Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }


        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void miAdd_Click(object sender, RoutedEventArgs e)
        {
            CustomDialog dlg = new CustomDialog(null);
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                try
                {
                    db.CreateCar(dlg.NewCar);

                    lvCars.ItemsSource = db.GetAllCars();
                    txtStatus.Text = "New Car Added: " + dlg.NewCar.ToString();
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show(this,
                        "Database Connection Fail",
                        "Database Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }

        private void miExport_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            if (saveDialog.ShowDialog() == true)
            {
                string fName = saveDialog.FileName;
                if(!Regex.IsMatch(fName, @".+\.csv", RegexOptions.IgnoreCase))
                {
                    fName += ".csv";
                }
                //SaveDataToFile(fName);
                ExportDataToCSVFile(fName);
                txtStatus.Text = "Data Exported: " + fName;
            }
        }

        void ExportDataToCSVFile(string fileName)
        {
            try
            {
                using(StreamWriter sw = new StreamWriter(fileName))
                using(CsvWriter cw = new CsvWriter(sw, CultureInfo.InvariantCulture))
                {
                    cw.WriteRecords(carList); 
                }
            }catch (IOException ex)
            {
                MessageBox.Show(
                    this,
                    "Cannot save data",
                    "Saving data Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
        private void lvCars_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditCar();
        }
        private void lvCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtStatus.Text = "You have " + carList.Count + " car(s) currently";
        }

        private void lvCars_MouseRightButtonClick(object sender, MouseButtonEventArgs e)
        {
            if(lvCars.SelectedIndex == -1) { return;  }

            ContextMenu cm = this.FindResource("rClickMenu") as ContextMenu;
            cm.PlacementTarget = sender as ListView;
            cm.IsOpen = true;
        }

        private void rClickMenu_miEdit_Click(object sender, RoutedEventArgs e)
        {
            int selIndex = lvCars.SelectedIndex;
            if (selIndex == -1)
            {
                MessageBox.Show(this,
                    "This shouldn't happen",
                    "Internal Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            EditCar();
        }

        private void rClickMenu_miDelete_Click(object sender, RoutedEventArgs e)
        {
            Car selCar = (Car)lvCars.SelectedItem;
            if(selCar == null)
            {
                MessageBox.Show(this,
                    "This shouldn't happen",
                    "Internal Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            db.DeleteCar(selCar.Id);
            lvCars.ItemsSource = db.GetAllCars();
            txtStatus.Text = "Deleted Item: " + selCar;
        }
    }
}
