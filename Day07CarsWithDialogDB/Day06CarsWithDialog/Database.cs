﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Day07CarsWithDialogDB;

namespace Day06CarsWithDialog
{
    public class Database
    {
        private static SqlConnection conn;
        public Database()
        {
            if(conn == null) {
                conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\heok\School\dotnet\Day07CarsWithDialogDB\CarsDB.mdf;Integrated Security=True;Connect Timeout=30");
                conn.Open();
            }
        }

        public List<Car> GetAllCars()
        {
            List<Car> carList = new List<Car>();
            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM Cars", conn);
            SqlDataReader sqlReader = cmdSelectAll.ExecuteReader();
            while (sqlReader.Read())
            {
                int id = sqlReader.GetInt32(sqlReader.GetOrdinal("Id"));
                string makeModel = sqlReader.GetString(sqlReader.GetOrdinal("MakeModel"));
                double engineSizeL = sqlReader.GetDouble(sqlReader.GetOrdinal("EngineSizeL"));
                string fuelType = sqlReader.GetString(sqlReader.GetOrdinal("FuelType"));

                try {
                    carList.Add(new Car(id, makeModel, engineSizeL, fuelType));
                } catch (ArgumentException ex)
                {
                    Console.WriteLine("[Error]Invalid data from database: "+ ex.Message);
                }
            }
            return carList;
        }

        public void UpdateCar(Car car)
        {
            SqlCommand cmdUpdate = new SqlCommand(
                "UPDATE Cars SET " +
                    "MakeModel=@MakeModel, " +
                    "EngineSizeL=@EngineSizeL, " +
                    "FuelType=@FuelType " +
                "WHERE Id=@Id",
                conn
                );
            cmdUpdate.Parameters.AddWithValue("@MakeModel", car.MakeModel);
            cmdUpdate.Parameters.AddWithValue("@EngineSizeL", car.EngineSizeL);
            cmdUpdate.Parameters.AddWithValue("@FuelType", car.FuelType.ToString());
            cmdUpdate.Parameters.AddWithValue("@Id", car.Id);
            cmdUpdate.ExecuteNonQuery();
        }

        public void CreateCar(Car car)
        {
            SqlCommand cmdCreate = new SqlCommand(
                "INSERT INTO Cars (MakeModel,EngineSizeL,FuelType)" +
                " VALUES (@MakeModel,@EngineSizeL,@FuelType)",
                conn);
            cmdCreate.Parameters.AddWithValue("@MakeModel", car.MakeModel);
            cmdCreate.Parameters.AddWithValue("@EngineSizeL", car.EngineSizeL);
            cmdCreate.Parameters.AddWithValue("@FuelType", car.FuelType.ToString());
            cmdCreate.ExecuteNonQuery();
        }

        public void DeleteCar(int Id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Cars WHERE Id=@Id", conn);
            cmdDelete.Parameters.AddWithValue("@Id", Id);
            cmdDelete.ExecuteNonQuery();
        }
    }
}
