﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day11CarsOwnersEF
{
    public class Car
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string MakeModel { get; set; }

        public int OwnerId { get; set; }

        [Required]
        public virtual Owner Owner { get; set; }
    }
}
