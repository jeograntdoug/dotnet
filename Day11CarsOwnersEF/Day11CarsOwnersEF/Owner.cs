﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Day11CarsOwnersEF
{
    public class Owner
    {
        public Owner() {
            Cars = new HashSet<Car>();
        }
        public Owner (string name, byte[] photo) :this()
        {
            if(name == null || photo == null)
            {
                throw new ArgumentException("All fields is required.");
            }
            Name = name;
            Photo = photo;

        }
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [NotMapped]
        public int CarsNumber { get{ return Cars.Count; } }

        public byte[] Photo { get; set; }

        public virtual ICollection<Car> Cars { get; set; }
    }
}
