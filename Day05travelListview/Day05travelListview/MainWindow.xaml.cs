﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace Day05travelListview
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Trip> tripList = new List<Trip>();
        const string DefaultFilePath = @"..\..\trip.txt";
        public MainWindow()
        {
            InitializeComponent();

            loadDataFromFile();

            lvTrip.ItemsSource = tripList;
        }

        private void btAddTrip_Click(object sender, RoutedEventArgs e)
        {
            string dest = tbDestination.Text;
            string name = tbName.Text;
            string passNo = tbPassportNo.Text;

            DateTime? departDate = dpDeparture.SelectedDate;
            DateTime? returnDate = dpReturn.SelectedDate;

            try {
                tripList.Add(new Trip(dest, name, passNo, departDate, returnDate));
                lvTrip.Items.Refresh();
            } catch (ArgumentException ex)
            {
                MessageBox.Show(
                    this, 
                    "Cannot Add New Trip.\n Error: " + ex.Message, 
                    "Adding trip fail", 
                    MessageBoxButton.OK
                    );
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(tripList.Count > 0) {
                saveDataToFile(tripList, DefaultFilePath);
            }
        }

        private void saveDataToFile(List<Trip> list, string filePath)
        {
            try
            {
                using(  TextWriter tw = File.CreateText(filePath))
                {
                    foreach( Trip t in list)
                    {
                        tw.WriteLine(t.ToDataString());
                    }
                }
            } catch (IOException ex)
            {
                MessageBox.Show(this, "Cannot save data.", "Saving Fail", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void loadDataFromFile()
        {
            try
            {
                using(TextReader tr = new StreamReader(DefaultFilePath))
                {
                    string dataLine = tr.ReadLine();
                    while(dataLine != null)
                    {
                        try { 
                            tripList.Add(new Trip(dataLine));
                            dataLine = tr.ReadLine();
                        } catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }

                }
            } catch (IOException ex)
            {
                MessageBox.Show(this, "Cannot open file", "Loading Fail", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btSaveSelected_Click(object sender, RoutedEventArgs e)
        {

            List<Trip> selectedTrips = lvTrip.SelectedItems.Cast<Trip>().ToList();

            if (tripList.Count == 0)
            {
                MessageBox.Show(this,
                    "There is no trip in the list.\nPlease add a trip.",
                    "Empty list",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            if (selectedTrips.Count == 0)
            {
                MessageBox.Show(this,
                    "Please select trips you want to save.",
                    "None selected",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning);
                return;
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true) {
                string fileName = saveFileDialog.FileName;
                saveDataToFile(selectedTrips, fileName);
            }

        }

        private void btUpdateTrip_Click(object sender, RoutedEventArgs e)
        {
            int selIndex = lvTrip.SelectedIndex;
            if (selIndex == -1)
            {
                MessageBox.Show(
                    this, 
                    "Internal Error(Button must be disalbed).", 
                    "Internal Error", 
                    MessageBoxButton.OK, 
                    MessageBoxImage.Error
                );
                return;
            }

            try {
                Trip newTrip = new Trip(
                    tbDestination.Text,
                    tbName.Text,
                    tbPassportNo.Text,
                    dpDeparture.SelectedDate,
                    dpReturn.SelectedDate 
                );
                tripList.RemoveAt(selIndex);
                tripList.Insert(selIndex, newTrip);
                lvTrip.Items.Refresh();
            } catch(ArgumentException ex)
            {
                MessageBox.Show(this,
                    "Fail update: " + ex.Message,
                    "Updating Fail",
                    MessageBoxButton.OK,
                    MessageBoxImage.Warning
                    );
            }

        }
        private void btDeleteTrip_Click(object sender, RoutedEventArgs e)
        {
            int selIndex = lvTrip.SelectedIndex;
            if (selIndex == -1)
            {
                MessageBox.Show(
                    this,
                    "Internal Error(Button must be disalbed).",
                    "Internal Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error
                );
                return;
            }
            tripList.RemoveAt(selIndex);
            lvTrip.Items.Refresh();
        }

        private void lvTrip_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Trip selectedItem = (Trip)lvTrip.SelectedItem;
            if (selectedItem == null)
            {
                btUpdateTrip.IsEnabled = false;
                btDeleteTrip.IsEnabled = false;
                tbDestination.Text = "";
                tbName.Text = "";
                tbPassportNo.Text = "";
                dpDeparture.SelectedDate = null;
                dpReturn.SelectedDate = null;
                return;
            }

            btUpdateTrip.IsEnabled = true;
            btDeleteTrip.IsEnabled = true;
            tbDestination.Text = selectedItem.Destination;
            tbName.Text = selectedItem.TravellerName;
            tbPassportNo.Text = selectedItem.TravellerPassportNo;
            dpDeparture.SelectedDate = selectedItem.DepartureDate;
            dpReturn.SelectedDate = selectedItem.ReturnDate;
        }
    }
}
