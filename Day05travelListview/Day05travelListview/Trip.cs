﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day05travelListview
{
    class Trip
    {
        string _destination;
        string _travellerName;
        string _travellerPassportNo;
        DateTime _departureDate, _returnDate;
        
        static string dateToStringFormat = "MMM d, yyyy";
        public static string dateFormat = "MM/dd/yyyy";

        public Trip(
            string destination,
            string travellerName,
            string travellerPassportNo, 
            DateTime? departureDate, 
            DateTime? returnDate )
        {
            Destination = destination;
            TravellerName = travellerName;
            TravellerPassportNo = travellerPassportNo;
            setDepartureAndReturnDates(departureDate, returnDate);
        }

        public Trip(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if(data.Length != 5)
            {
                throw new ArgumentException("Invalid Data Line: " + dataLine);
            }

            Destination = data[0];
            TravellerName = data[1];
            TravellerPassportNo = data[2];
            setDepartureAndReturnDates(data[3], data[4]);
        }
        public string Destination
        {
            get { return _destination; }
            set
            {
                if (!Regex.IsMatch(value,"[^;]{2,30}"))
                {
                    throw new ArgumentException("Destination must be 2-30 characters, no semicolons.");
                }
                _destination = value;
            }
        }

        public string TravellerName
        {
            get { return _travellerName; }
            set
            {
                if (!Regex.IsMatch(value, "[^;]{2,30}"))
                {
                    throw new ArgumentException("Traveller's Name must be 2-30 characters, no semicolons.");
                }
                _travellerName = value;
            }
        }

        public string TravellerPassportNo
        {
            get { return _travellerPassportNo; }
            set
            {
                if (!Regex.IsMatch(value, "[A-Z]{2}[0-9]{8}"))
                {
                    throw new ArgumentException("Passport No. must be two uppercase letters followed by 8 digts");
                }
                _travellerPassportNo = value;
            }
        }

        public DateTime DepartureDate
        {
            get { return _departureDate; }
        }

        public DateTime ReturnDate
        {
            get { return _returnDate; }
        }

        public void setDepartureAndReturnDates(DateTime? depDate, DateTime? retDate) {
         
            if(!isValidDate(depDate) || !isValidDate(retDate))
            {
                throw new ArgumentException("Date must be year 1900-2100");
            }
            if (depDate.Value > retDate.Value)
            {
                throw new ArgumentException("Return Date must be after Departure Date");
            }

            _departureDate = depDate.Value;
            _returnDate = retDate.Value;
        }
        public void setDepartureAndReturnDates(string depDate, string retDate)
        {
            DateTime dep, ret;
            if(!DateTime.TryParseExact(depDate,dateFormat, null, DateTimeStyles.None ,out dep) 
                || !DateTime.TryParseExact(retDate, dateFormat, null, DateTimeStyles.None , out ret))
            {
                throw new ArgumentException("Date must be year 1900-2100");
            }

            setDepartureAndReturnDates(dep, ret);
        }

        private Boolean isValidDate(DateTime? date)
        {
            if(date == null)
            {
                return false;
            }
            if (date.Value.Year > 2100 || date.Value.Year < 1900)
            {
                return false;
            }
            return true;
        }

        public string ToDataString()
        {
            return string.Format(
                "{0};{1};{2};{3};{4}",
                Destination, 
                TravellerName, 
                TravellerPassportNo, 
                DepartureDate.ToString(dateFormat), 
                ReturnDate.ToString(dateFormat)
            );
        }
        public override string ToString()
        {
            return string.Format(
                "{0}({1}) to {2} on {3}",
                TravellerName,TravellerPassportNo,Destination,
                DepartureDate.ToString(dateToStringFormat)
                );
        }
    }
}
