﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04ListView.Properties
{
    class Person
    {
        public string Name;
        public int Age;


        public override string ToString()
        {
            return string.Format("{0} is {1} y/o", Name, Age);
        }
    }
}
