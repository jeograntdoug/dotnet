﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Day04ListView.Properties;

namespace Day04ListView
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Person> peopleList = new List<Person>();

        public MainWindow()
        {
            InitializeComponent();
            lvPeople.DataContext = peopleList;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            string name = tbName.Text;
            string ageStr = tbAge.Text;
            int age;
            if (!int.TryParse(ageStr, out age))
            {
                MessageBox.Show(this, "Please insert a valid age. Age should be a number.", "Input Data Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            lvPeople.Items.Add(new Person() { Name = name, Age = age });
            resetField();
        }

        private void resetField()
        {
            tbName.Text = "";
            tbAge.Text = "";
            lvPeople.SelectedItem = null;
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {

        }

        private void lvPeople_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lvPeople.SelectedItem == null)
            {

            }
        }
    }
}
