﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz2Passengers
{
    public class Passenger
    {
        public int Id { get; set; } // INT PK IDENTITY(1,1)
        string _name;// VC(100) NOT NULL
        string _passportNo; // VC(10) NOT NULL
        string _destination; // VC(100) NOT NULL
        DateTime _departureDateTime; // DateTime type, visible as two inputs in UI

        public Passenger(int id, string name, string passportNo, string destination, DateTime departureDateTime)
        {
            Id = id;
            Name = name;
            PassportNo = passportNo;
            Destination = destination;
            DepartureDateTime = departureDateTime;
        }

        public Passenger(string name, string passportNo, string destination, DateTime? depDate, string depTime)
        {
            Name = name;
            PassportNo = passportNo;
            Destination = destination; 
            
            if (depDate == null)
            {
                throw new ArgumentException("Select departure Date.");
            }
            setDateTime(depDate.Value, depTime);
        }
        public string Name
        {
            get { return _name; }
            set
            {
                if(value.Length > 100)
                {
                    throw new ArgumentException("Name must be 0~100 characters.");
                }
                _name = value;
            }
        }

        public string PassportNo
        {
            get { return _passportNo; }
            set
            {
                if (!Regex.IsMatch(value, "^[A-Z]{2}[0-9]{6}$"))
                {
                    throw new ArgumentException("Passport No must be AB123456 format.");
                }
                _passportNo = value;
            }
        }

        public string Destination
        {
            get { return _destination; }
            set
            {
                if(value.Length > 100)
                {
                    throw new ArgumentException("Destination must be 0~100 characters.");
                }
                _destination = value;
            }
        }

        public DateTime DepartureDateTime
        {
            get { return _departureDateTime; }
            set
            {
                if ( (value.Minute != 30 && value.Minute != 0)
                    || value.Second != 0 || value.Millisecond != 0)
                {
                    throw new ArgumentException("DepartureDateTime must be from the list");
                }
                _departureDateTime = value;
            }
        }

        void setDateTime(DateTime depDate, string depTime)
        {
            if(depTime == null)
            {
                throw new ArgumentException("Please Select Dep Time");

            }
            if (!Regex.IsMatch(depTime, "^[0-9]{1,2}:[0-9]{1,2}$"))
            {
                throw new ArgumentException("Internal Error: Invalid departure Time");
            }

            DateTime depDateTime;
            if (!DateTime.TryParse(depDate.ToShortDateString() + " " + depTime, out depDateTime))
            {
                throw new ArgumentException("Internal Error: Invalid departure Date/Time");
            }

            _departureDateTime = depDateTime;
        }

        public bool filterValue(string filterStr)
        {
            if (Regex.IsMatch(Id.ToString(), filterStr)) { return true; }
            if (Regex.IsMatch(Name, filterStr)) { return true; }
            if (Regex.IsMatch(PassportNo, filterStr)) { return true; }
            if (Regex.IsMatch(Destination, filterStr)) { return true; }
            if (Regex.IsMatch(DepartureDateTime.Year.ToString(), filterStr)) { return true; }
            if (Regex.IsMatch(DepartureDateTime.Month.ToString(), filterStr)) { return true; }
            if (Regex.IsMatch(DepartureDateTime.Hour.ToString(), filterStr)) { return true; }
            if (Regex.IsMatch(DepartureDateTime.Minute.ToString(), filterStr)) { return true; }

            return false;
        }
    }
}
