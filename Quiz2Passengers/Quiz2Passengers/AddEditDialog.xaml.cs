﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        private Passenger crtPassenger;
        public AddEditDialog(Passenger passenger)
        {
            InitializeComponent();

            crtPassenger = passenger;
            
            List<string> depTimeList = new List<string>();
            for (int hour = 0; hour < 24; hour++)
            {
                depTimeList.Add(string.Format("{0:00}:{1:00}", hour, 0));
                depTimeList.Add(string.Format("{0:00}:{1:00}", hour, 30));
            }
            comboDepTime.ItemsSource = depTimeList;

            if (crtPassenger == null)
            {
                txtId.Text = "-";
                tbName.Text = "";
                tbPassNo.Text = "";
                tbDest.Text = "";
                dpDepDate.SelectedDate = null;
                comboDepTime.SelectedIndex = -1;
                btDelete.IsEnabled = false;
                btAddEdit.Content = "Add";
            } else
            {
                txtId.Text = passenger.Id.ToString();
                tbName.Text = passenger.Name;
                tbPassNo.Text = passenger.PassportNo;
                tbDest.Text = passenger.Destination;
                dpDepDate.SelectedDate = passenger.DepartureDateTime;
                comboDepTime.SelectedItem = passenger.DepartureDateTime.ToString("HH:mm");//FIXME
                btDelete.IsEnabled = true;
                btAddEdit.Content = "Edit";
            }

            
        }

        public Passenger NewPassenger { get; set; }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btAddEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                NewPassenger = new Passenger(
                        tbName.Text,
                        tbPassNo.Text,
                        tbDest.Text,
                        dpDepDate.SelectedDate,
                        (string)comboDepTime.SelectedItem
                    );

                this.DialogResult = true;
            }catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    ex.Message,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            NewPassenger = null;
            this.DialogResult = true;
        }
    }
}
