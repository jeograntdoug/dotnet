﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2Passengers
{
    class Database
    {
        static SqlConnection conn;

        public Database() //ex
        {
            if(conn == null)
            {
                conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\heok\School\dotnet\Quiz2Passengers\PassengerDB.mdf;Integrated Security=True;Connect Timeout=30");//ex
                conn.Open();
            }
        }

        public List<Passenger> GetAllPassengers()// ex SqlException, ArgumentException
        {
            List<Passenger> passengerList = new List<Passenger>();

            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM Passengers", conn);

            using (SqlDataReader reader = cmdSelectAll.ExecuteReader())
            { // ex SqlException
                while (reader.Read())
                {
                    passengerList.Add(new Passenger(    // ex ArgumentException
                        reader.GetInt32(reader.GetOrdinal("Id")),
                        reader.GetString(reader.GetOrdinal("Name")),
                        reader.GetString(reader.GetOrdinal("PassportNo")),
                        reader.GetString(reader.GetOrdinal("Destination")),
                        reader.GetDateTime(reader.GetOrdinal("DepartureDateTime"))
                        ));
                }
                return passengerList;
            }
        }

        public void AddPassenger(Passenger newP) // ex SqlException
        {
            SqlCommand cmdInsert = new SqlCommand(
                "INSERT INTO Passengers (Name, PassportNo, Destination, DepartureDateTime) " +
                "VALUES(@Name, @PassportNo, @Destination, @DepartureDateTime)", conn);
            cmdInsert.Parameters.AddWithValue("@Name",newP.Name);
            cmdInsert.Parameters.AddWithValue("@PassportNo",newP.PassportNo);
            cmdInsert.Parameters.AddWithValue("@Destination",newP.Destination);
            cmdInsert.Parameters.AddWithValue("@DepartureDateTime",newP.DepartureDateTime);
            cmdInsert.ExecuteNonQuery();
        }

        public void EditPassenger(Passenger newP) // ex SqlException
        {
            SqlCommand cmdUpdate = new SqlCommand(
                "UPDATE Passengers " +
                "SET Name=@Name, PassportNo=@PassportNo, Destination=@Destination, DepartureDateTime=@DepartureDateTime " +
                "WHERE Id=@Id", conn);
            cmdUpdate.Parameters.AddWithValue("@Name", newP.Name);
            cmdUpdate.Parameters.AddWithValue("@PassportNo", newP.PassportNo);
            cmdUpdate.Parameters.AddWithValue("@Destination", newP.Destination);
            cmdUpdate.Parameters.AddWithValue("@DepartureDateTime", newP.DepartureDateTime);
            cmdUpdate.Parameters.AddWithValue("@Id", newP.Id);
            cmdUpdate.ExecuteNonQuery();
        }

        public void DeletePassenger(int id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Passengers WHERE Id = @Id", conn);
            cmdDelete.Parameters.AddWithValue("@Id", id);
            cmdDelete.ExecuteNonQuery();
        }
    }
}
