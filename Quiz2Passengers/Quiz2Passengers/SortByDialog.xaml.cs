﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for SortByDialog.xaml
    /// </summary>
    public partial class SortByDialog : Window
    {
        public SortByDialog(MainWindow mainWindow)
        {
            InitializeComponent();

            switch (mainWindow.CurrSortOrder)
            {
                case MainWindow.SortOrderEnum.Name:
                    rbSortByName.IsChecked = true;
                    break;
                case MainWindow.SortOrderEnum.PassNo:
                    rbSortByPassNo.IsChecked = true;
                    break;
                case MainWindow.SortOrderEnum.Dest:
                    rbSortByDest.IsChecked = true;
                    break;
                case MainWindow.SortOrderEnum.DeptDateTime:
                    rbSortByDepDateTime.IsChecked = true;
                    break;
                default:
                    MessageBox.Show(this,
                        "Internal Error: Check <CurrSortOrder>.",
                        "Internal Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    break;
            }

            SortBy = mainWindow.CurrSortOrder;
        }
        
        public MainWindow.SortOrderEnum SortBy { set; get; }

        private void rbSortByName_Checked(object sender, RoutedEventArgs e)
        {
            SortBy = MainWindow.SortOrderEnum.Name;
        }

        private void rbSortByPassNo_Checked(object sender, RoutedEventArgs e)
        {
            SortBy = MainWindow.SortOrderEnum.PassNo;
        }

        private void rbSortByDest_Checked(object sender, RoutedEventArgs e)
        {
            SortBy = MainWindow.SortOrderEnum.Dest;
        }

        private void rbSortByDepDateTime_Checked(object sender, RoutedEventArgs e)
        {
            SortBy = MainWindow.SortOrderEnum.DeptDateTime;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btApply_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
