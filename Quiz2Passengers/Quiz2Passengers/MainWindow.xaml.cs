﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2Passengers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Database db;
        public enum SortOrderEnum { Name, PassNo, Dest, DeptDateTime }

        // PROBABLY NOT NEEDED: List<Passenger> passengerList;

        public SortOrderEnum CurrSortOrder;
        public MainWindow()
        {
            InitializeComponent();

            CurrSortOrder = SortOrderEnum.Name;

            try
            {
                db = new Database();// ex SqlException
                refreshList(); // ex SqlException
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    "Database validation doesn't match with Program validation.\n" + ex.Message,
                    "Internal Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }catch (SqlException ex)
            {
                MessageBox.Show(this,
                    "Database connection fail: " + ex.Message,
                    "Database Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
        }

        private List<Passenger> GetFilteredList(List<Passenger> list)
        {
            string filter = tbSearchFilter.Text;

            return new List<Passenger>(
                    from p in list
                    where p.filterValue(filter)
                    select p);
        }
        private void refreshList()//ex SqlException
        {
            List<Passenger> filteredList = GetFilteredList(db.GetAllPassengers());//ex SqlException

            switch (CurrSortOrder)
            {
                case SortOrderEnum.Name:
                    lvPassengers.ItemsSource =
                        from p in filteredList
                           orderby p.Name
                           select p;
                    return;
                case SortOrderEnum.PassNo:
                    lvPassengers.ItemsSource =
                        from p in filteredList
                        orderby p.PassportNo
                        select p;
                    return;
                case SortOrderEnum.Dest:
                    lvPassengers.ItemsSource =
                        from p in filteredList
                        orderby p.Destination
                        select p;
                    return;
                case SortOrderEnum.DeptDateTime:
                    lvPassengers.ItemsSource =
                        from p in filteredList
                        orderby p.DepartureDateTime
                        select p;
                    return;
                default:
                    MessageBox.Show(this,
                        "Internal Error: Check <CurrSortOrder>.",
                        "Internal Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
            }
        }

        private void tbSearchFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            try {
                refreshList();//ex SqlException
            } catch (SqlException ex)
            {
                MessageBox.Show(this,
                    "Database connection fail: " + ex.Message,
                    "Database Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
        }

        private void btSortBy_Click(object sender, RoutedEventArgs e)
        {
            SortByDialog sortByDialog = new SortByDialog(this);
            if(sortByDialog.ShowDialog() == true)
            {
                try {
                    CurrSortOrder = sortByDialog.SortBy;
                    refreshList(); //ex SqlException
                } catch (SqlException ex)
                {
                    MessageBox.Show(this,
                    "Database connection fail: " + ex.Message,
                    "Database Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                    return;
                }
            }
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditDlg = new AddEditDialog(null);
            if (addEditDlg.ShowDialog() == true)
            {
                Passenger newP = addEditDlg.NewPassenger;
                
                db.AddPassenger(newP);
                refreshList();//ex SqlException
            }
        }

        private void lvPassengers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Passenger selP = (Passenger)lvPassengers.SelectedItem;
            if(selP == null) { return; }
            AddEditDialog addEditDlg = new AddEditDialog(selP);

            if (addEditDlg.ShowDialog() == true)
            {
                Passenger newP = addEditDlg.NewPassenger;
                if (newP == null) // Delete button clicked
                {
                    db.DeletePassenger(selP.Id);
                    
                } else {
                    newP.Id = selP.Id;
                    db.EditPassenger(newP);
                }
                refreshList();
            }
        }

    }
}
