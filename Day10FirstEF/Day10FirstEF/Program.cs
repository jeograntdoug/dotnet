﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Cache;
using System.Text;
using System.Threading.Tasks;

namespace Day10FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            SocietyDBContext ctx = new SocietyDBContext();
            Person p1 = new Person() { Name = "Jerry", Age = random.Next(100), Gender = Person.GenderEnum.Female };
            ctx.People.Add(p1);
            ctx.SaveChanges();
            Console.WriteLine("Record Added");

            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
    }
}
