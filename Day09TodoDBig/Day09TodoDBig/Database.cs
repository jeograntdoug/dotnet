﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09TodoDBig
{
    class Database
    {
        private static SqlConnection conn;

        public Database()
        {
            if(conn == null) {
                conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\heok\School\dotnet\Day09TodoDBig\TodoDB.mdf;Integrated Security=True;Connect Timeout=30");
                conn.Open();
            }
        }

        public List<Todo> GetAllTodos() // ex : ArgumentException, SqlException
        {
            List<Todo> list = new List<Todo>();

            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM Todos", conn);
            using (SqlDataReader reader = cmdSelectAll.ExecuteReader())
            { // ex: SqlException
                while (reader.Read())
                {
                    int id = reader.GetInt32(reader.GetOrdinal("Id"));
                    string task = reader.GetString(reader.GetOrdinal("Task"));
                    DateTime dueDate = reader.GetDateTime(reader.GetOrdinal("DueDate"));
                    string status = reader.GetString(reader.GetOrdinal("Status"));

                    list.Add(new Todo(id, task, dueDate, status)); // ex: ArgumentException
                }
                return list;
            }
        }

        public void CreateTodo(Todo newTodo)
        {
            SqlCommand cmdInsert = new SqlCommand(
                "INSERT INTO Todos (Task, DueDate, Status) " +
                "VALUES(@Task, @DueDate, @Status)" , conn
                );
            cmdInsert.Parameters.AddWithValue("@Task", newTodo.Task);
            cmdInsert.Parameters.AddWithValue("@DueDate", newTodo.DueDate);
            cmdInsert.Parameters.AddWithValue("@Status", newTodo.Status.ToString());
            cmdInsert.ExecuteNonQuery();
        }

        public void UpdateTodo(Todo newTodo)
        {
            SqlCommand cmdUpdate = new SqlCommand(
                "UPDATE Todos " +
                "SET Task=@Task, DueDate=@DueDate, Status=@Status " +
                "WHERE Id=@Id", conn
                );
            cmdUpdate.Parameters.AddWithValue("@Task", newTodo.Task);
            cmdUpdate.Parameters.AddWithValue("@DueDate", newTodo.DueDate);
            cmdUpdate.Parameters.AddWithValue("@Status", newTodo.Status.ToString());
            cmdUpdate.Parameters.AddWithValue("@Id", newTodo.Id);
            cmdUpdate.ExecuteNonQuery();
        }

        public void DeleteTodo(int id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Todos WHERE Id=@Id", conn);
            cmdDelete.Parameters.AddWithValue("@Id", id);
            cmdDelete.ExecuteNonQuery();
        }
    }
}
