﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day09TodoDBig
{
    public class Todo
    {
        public Todo(int id, string task, DateTime dueDate, string statusStr)
        {
            Id = id;
            Task = task;
            DueDate = dueDate;
            setStatusStr(statusStr);
        }
        public Todo(int id, string task, DateTime? dueDate, StatusEnum status)
        {
            Id = id;
            Task = task;
            if(dueDate == null)
            {
                throw new ArgumentException("Please select due date.");
            }
            DueDate = dueDate.Value;
            Status = status;
        }
        public enum StatusEnum { Done,Pending }
        public int Id { get; set; }
        string _task;
        public string Task
        {
            get { return _task; }
            set
            {
                if(value.Length > 50 || value.Length < 1)
                {
                    throw new ArgumentException("Task Must be 1~50 characters");
                }
                _task = value;
            }
        }

        DateTime _dueDate;

        // For Export(CSV) data format
        public DateTime DueDate
        {
            get { return _dueDate; }
            set
            {
                if(value.Year < 1900 || value.Year > 2100)
                {
                    throw new ArgumentException("DueDate Must be 1900~ 2100");
                }
                DueDateStr = value.ToString("yyyy-MM-dd");
                _dueDate = value;
            }
        }
        public string DueDateStr { get; set; }
        public StatusEnum Status { get; set; }
        public void setStatusStr(string statusStr)
        {
            try{
                StatusEnum status = (StatusEnum) Enum.Parse(typeof(StatusEnum), statusStr);
                Status = status;
            } catch ( ArgumentException ex)
            {
                throw new ArgumentException("Status must be Pending or Done");
            }
        }
    }
}
