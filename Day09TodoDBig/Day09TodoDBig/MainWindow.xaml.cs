﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CsvHelper;
using CsvHelper.Configuration;
using Microsoft.Win32;

namespace Day09TodoDBig
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Database db;
        List<Todo> todoList;
        public MainWindow()
        {

            InitializeComponent();

            try
            {
                db = new Database();
                todoList = db.GetAllTodos();
                lvTodos.ItemsSource = todoList;
                rbSortByTask.IsChecked = true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    "Database data doesn't match with program data:\n " + ex.Message,
                    "Database Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this,
                    "Cannot Connect database.",
                    "Database Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void miExport_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "csv files (*.csv)|*.csv";
            if (saveDlg.ShowDialog() == true)
            {
                try { 
                    using (var writer = new StreamWriter(saveDlg.FileName))
                    using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
                    {
                        csv.Configuration.RegisterClassMap<CustomCSVMap>();
                        csv.WriteRecords(lvTodos.ItemsSource);
                    }
                    txtStatus.Text = "Export Data to: " + saveDlg.FileName;
                } catch (IOException ex)
                {
                    MessageBox.Show(this,
                        "Cannot Export data",
                        "Export Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                }
            }
        }

        private void rbSortByTask_Checked(object sender, RoutedEventArgs e)
        {
            todoList = new List<Todo>( from todo in todoList
                       orderby todo.Task
                       select todo);
            lvTodos.ItemsSource = todoList;
        }

        private void rbSortByDueDate_Checked(object sender, RoutedEventArgs e)
        {
            todoList = new List<Todo>(from todo in todoList
                        orderby todo.DueDate
                        select todo);
            lvTodos.ItemsSource = todoList;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            AddEditDialog dlg = new AddEditDialog(null);
            if(dlg.ShowDialog() == true)
            {
                try {
                    db.CreateTodo(dlg.NewTodo);
                    todoList = db.GetAllTodos();
                    lvTodos.ItemsSource = todoList;
                } catch (SqlException ex)
                {
                    MessageBox.Show(this,
                    "Cannot Connect database.",
                    "Database Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                    return;
                }
            }
        }

        private void lvTodos_MouseRightButtonClick(object sender, MouseButtonEventArgs e)
        {
            if(lvTodos.SelectedIndex == -1) { return; }

            ContextMenu cm = this.FindResource("rClickMenu") as ContextMenu;
            cm.PlacementTarget = sender as ListView;
            cm.IsOpen = true;
        }

        private void miDelete_Click(object sender, RoutedEventArgs e)
        {
            Todo selTodo = (Todo)lvTodos.SelectedItem;
            if (selTodo == null) { return; }

            try
            {
                db.DeleteTodo(selTodo.Id);
                todoList = db.GetAllTodos();
                lvTodos.ItemsSource = todoList;
            }
            catch (SqlException ex)
            {
                MessageBox.Show(this,
                "Cannot Connect database.",
                "Database Error",
                MessageBoxButton.OK,
                MessageBoxImage.Error);
                return;
            }
        }

        private void lvTodos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Todo selTodo = (Todo)lvTodos.SelectedItem;
            if (selTodo == null) { return; }

            AddEditDialog dlg = new AddEditDialog(selTodo);

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    db.UpdateTodo(dlg.NewTodo);
                    todoList = db.GetAllTodos();
                    lvTodos.ItemsSource = todoList;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show(this,
                    "Cannot Connect database.",
                    "Database Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                    return;
                }
            }
        }
    }

    // For Export(CSV) configuration
    class CustomCSVMap : ClassMap<Todo>
    {
        public CustomCSVMap()
        {
            Map(m => m.Id).Name("Id");
            Map(m => m.Task).Name("Task");
            Map(m => m.DueDateStr).Name("Due Date");
            Map(m => m.Status).Name("Status");
        }
    }
}
