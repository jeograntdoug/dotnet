﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day09TodoDBig
{
    /// <summary>
    /// Interaction logic for AddEditDialog.xaml
    /// </summary>
    public partial class AddEditDialog : Window
    {
        public AddEditDialog(Todo todo)
        {
            InitializeComponent();

            if(todo != null)
            {
                txtId.Text = todo.Id.ToString();
                tbTask.Text = todo.Task;
                dpDueDate.SelectedDate = todo.DueDate;
                cbIsDone.IsChecked = todo.Status == Todo.StatusEnum.Done;
                btAddEdit.Content = "Edit";
                return;
            }

            txtId.Text = "-";
            tbTask.Text = "";
            dpDueDate.SelectedDate = null;
            cbIsDone.IsChecked = false;
            btAddEdit.Content = "Add";
        }

        private void btAddEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int id;
                if(!int.TryParse(txtId.Text,out id)){
                    id = 0;// Add case
                }

                NewTodo = new Todo(
                    id, tbTask.Text, dpDueDate.SelectedDate,
                    (cbIsDone.IsChecked == true) ? Todo.StatusEnum.Done : Todo.StatusEnum.Pending
                    );

                this.DialogResult = true;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    "Invalid Data: " + ex.Message,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        public Todo NewTodo { get; set; }
    }
}
