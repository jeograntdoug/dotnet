﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02PeopleAgain
{
    class InvalidParameterException : Exception
    {

        public delegate void LogFailedSetterDelegate(string reason);

        public static LogFailedSetterDelegate LogFailSet;

        public InvalidParameterException(string msg) : base(msg) {
            LogFailSet(msg);
        }
    }

    class Person
    {
        string _name; // 1-50 characters, no semicolons
        int _age; // 0-150

        protected Person() { }
        public Person(string dataLine) {
            if(dataLine == null)
            {
                throw new InvalidParameterException("Data cannot be null");
            }
            string[] data = dataLine.Split(';');
            if(data.Length < 2) {
                throw new InvalidParameterException("Data is not enough");
            }

            Name = data[0];
            AgeStr = data[1];
        }
        public Person(string name,int age) {
            Name = name;
            Age = age;
        }

        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                if (value.Length > 50 || value.Length == 0 || value.Contains(';'))
                {
                    throw new InvalidParameterException("Name must be 1-50 characters, no semicolons.");
                }
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if(value < 0 || value > 150)
                {
                    throw new InvalidParameterException("Age must be 0~150.");
                }
                _age = value;
            }
        }
        public string AgeStr
        {
            set
            {
                int age;
                if (!int.TryParse(value, out age))
                {
                    throw new InvalidParameterException("Age must be 0~150.");
                }
                Age = age;
            }
        }

        public virtual string ToDataString()
        {
            return string.Format("Person;{0};{1}", Name, Age);
        }

        public override string ToString()
        {
            return string.Format("{0} is {1} y/o", Name, Age);
        }
    }

    class Teacher : Person
    {
        string _subject; // 1-50 characters, no semicolons
        int _yearsOfExperience; // 0-100
        public Teacher(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if(data.Length != 4)
            {
                throw new InvalidParameterException("Data for teacher is not enough");
            }
            Name = data[0];
            AgeStr = data[1];
            Subject = data[2];
            YearsOfExperianceStr = data[3];
        }
        public Teacher(string name,int age, string subject, int yoe) : base(name, age)
        {
            Subject = subject;
            YearsOfExperiance = yoe;
        }

        public string Subject
        {
            get
            {
                return _subject;
            }
            set
            {
                if (value.Length == 0 || value.Length > 50 || value.Contains(';'))
                {
                    throw new InvalidParameterException("Subject must be `-50 characters, no semicolons.");
                }
                _subject = value;
            }
        }
        public string YearsOfExperianceStr
        {
            set
            {
                int year;
                if (!int.TryParse(value,out year))
                {
                    throw new InvalidParameterException("Years of experiance must be 0~100.");
                }
                YearsOfExperiance = year;
            }
        }
        public int YearsOfExperiance
        {
            get
            {
                return _yearsOfExperience;
            }
            set
            {
                if(value <0 || value > 100)
                {
                    throw new InvalidParameterException("Years of experiance must be 0~100.");
                }
                _yearsOfExperience = value;
            }
        }

        public override string ToDataString()
        {
            return string.Format("Teacher;{0};{1};{2};{3}",
                Name, Age, Subject, YearsOfExperiance
                );
        }

        public override string ToString()
        {
            return string.Format(
                "Teacher {0} is {1} y/o, teaching {2} for {3} years",
                Name, Age,Subject,YearsOfExperiance
                );
        }
    }

    class Student : Person
    {
        string _program; // 1-50 characters, no semicolons
        double _gpa; // 0-100
        public Student(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if(data.Length != 4)
            {
                throw new InvalidParameterException("Data for student is not enough.");
            }
            Name = data[0];
            AgeStr = data[1];
            Program = data[2];
            GpaStr = data[3];
        }
        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            Program = program;
            Gpa = gpa;
        }

        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length == 0 || value.Length > 50 || value.Contains(';'))
                {
                    throw new InvalidParameterException("Program must be 1~50 characters, no semicolons");
                }
                _program = value;
            }
        }
        
        public double Gpa
        {
            get
            {
                return _gpa;
            }
            set
            {
                if(value < 0 || value > 4.3)
                {
                    throw new InvalidParameterException("GPA must be 0~4.3.");
                }
                _gpa = value;
            }
        }
        public string GpaStr
        {
            set
            {
                double gpa;
                if(!double.TryParse(value,out gpa))
                {
                    throw new InvalidParameterException("GPA must be number.");
                }
                Gpa = gpa;
            }
        }

        public static double AverageGpaOfStudents(List<Student> studentArray)
        {
            double totalGpa = 0;
            foreach(Student s in studentArray)
            {
                totalGpa += s.Gpa;
            }

            return totalGpa / studentArray.Count;
        }

        public static double StandardDeviationGpaOfStudent(List<Student> studentArray)
        {
            double avgGpa = AverageGpaOfStudents(studentArray);
            double medianSquare = 0;
            foreach(Student s in studentArray)
            {
                medianSquare += Math.Pow((s.Gpa - avgGpa), 2);
            }
            return Math.Sqrt(medianSquare / studentArray.Count);
        }

        public static double MediamGpaOfStudents(List<Student> studentArray)
        {
            studentArray.Sort(CompareStudentByGpa);
            return studentArray[(int)(studentArray.Count/2)].Gpa;
        }

        private static int CompareStudentByGpa(Student s1, Student s2)
        {
            return s1.Gpa.CompareTo(s2.Gpa);
        }

        public override string ToDataString()
        {
            return string.Format("Student;{0};{1};{2};{3}", Name, Age, Program, Gpa);
        }

        public override string ToString()
        {
            return string.Format(
                "Student {0} is {1} y/o, studying {2}, GPA: {3}",
                Name, Age, Program, Gpa
                );
        }
    }

    class Program
    {
        const string FileInputPath = @"..\..\people.txt";
        const string FileOutputPath = @"..\..\byname.txt";


        static List<Person> peopleList = new List<Person>();

        static void Main(string[] args)
        {
            
            Console.WriteLine(
                @"Where would you like to log setters errors?
                1-screen only
                2-screen and file
                3-do not log
                Your choice:"
            );
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    InvalidParameterException.LogFailSet += LogErrorToTheScreen;
                    break;
                case "2":
                    InvalidParameterException.LogFailSet += LogErrorToTheFile;
                    InvalidParameterException.LogFailSet += LogErrorToTheScreen;
                    break;
                case "3":
                    InvalidParameterException.LogFailSet = null;
                    break;
                default:
                    InvalidParameterException.LogFailSet = null;
                    break;
            }

            ReadDataFromFile();

            Console.WriteLine("Print All list");
            foreach(Person p in peopleList)
            {
                Console.WriteLine(p);
            }
            
            //Part5
                
            Console.WriteLine("================");
            Console.WriteLine("Print Only Students");
            foreach (Person p in peopleList)
            {
                if (p is Student) {
                    Console.WriteLine(p);
                }
            }

            Console.WriteLine("================");
            Console.WriteLine("Print Only Teachers");
            foreach (Person p in peopleList)
            {
                if (p is Teacher)
                {
                    Console.WriteLine(p);
                }
            }

            Console.WriteLine("================");
            Console.WriteLine("Print rest");
            foreach (Person p in peopleList)
            {
                if (p.GetType().Name == "Person") {
                   Console.WriteLine(p);
                }
            }

            // Part6
            List<Person> list = peopleList.FindAll(x => (x is Student));
            List<Student> studentList = new List<Student>();
            foreach(Person p in list)
            {
                studentList.Add((Student)p);
            }

            Console.WriteLine("================");
            Console.WriteLine("Student List : ");
            foreach (Student s in studentList)
            {
                Console.WriteLine(s);
            }
            Console.WriteLine(
                "average student's GPA: {0:0.00}", 
                Student.AverageGpaOfStudents(studentList)
                );
            Console.WriteLine(
                "standard deviation of student's GPA: {0:0.00}",
                Student.StandardDeviationGpaOfStudent(studentList)
                );
            Console.WriteLine(
                "Mediam of student's GPA: {0:0.00}",
                Student.MediamGpaOfStudents(studentList)
                );

            SaveDataToFile();
            Console.Write("Press any key...");
            Console.ReadKey();
        }

        static void LogErrorToTheScreen(string reason)
        {
            Console.WriteLine(reason);
        }

        static void LogErrorToTheFile(string reason)
        {
            try { 
                using (StreamWriter sw = File.AppendText(@"..\..\logError.txt"))
                {
                    sw.WriteLine(reason);
                }
            }
            catch(IOException ex)
            {
                Console.WriteLine("Cannot open the file.");
            }
        }

        static void SaveDataToFile()
        {
            try
            {
                using(StreamWriter sw = new StreamWriter(File.Open(FileOutputPath, FileMode.Create)))
                {
                    var peopleOrderedByName = from p in peopleList orderby p.Name select p;
                    foreach(Person p in peopleOrderedByName)
                    {
                        sw.WriteLine(p.ToDataString());
                    }
                }
            }
            catch(IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void ReadDataFromFile()
        {
            try
            {
                string[] dataArray = File.ReadAllLines(FileInputPath);
                int count = 2;
                char[] seperator = { ';' };
                
                foreach(string dataLine in dataArray)
                {
                    try
                    {
                        string[] type = dataLine.Split(seperator, count, StringSplitOptions.None);

                        if (type.Length != 2)
                        {
                            throw new InvalidParameterException("Invalid Data format");
                        }

                        switch (type[0])
                        {
                            case "Person":
                                peopleList.Add(new Person(type[1]));
                                break;
                            case "Teacher":
                                peopleList.Add(new Teacher(type[1]));
                                break;
                            case "Student":
                                peopleList.Add(new Student(type[1]));
                                break;
                            default:
                                throw new InvalidParameterException("Invalid Data format");
                        }
                    } catch (InvalidParameterException ex)
                    {
                    }
                }

            } catch (IOException ex)
            {
                Console.WriteLine("Cannot open the file. Program exit.");
            }
        }
        
    }

}
