﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01SimpleFileIO
{
    class Program
    {
        const string NamesFileName = @"..\..\names.txt";

        static void Main(string[] args)
        {
            // hard way
            using (StreamWriter sw = File.AppendText(@"..\..\output.txt")) //ex
            {
                sw.WriteLine("Line appended");//ex
            }//sw is closed here

            // easy way
            string[] linesArray = { "Jerry", "Terry", "Barry" };

            File.AppendAllLines(NamesFileName, linesArray); // ex

            string[] dataBack = File.ReadAllLines(NamesFileName); // ex

            string allText = File.ReadAllText(NamesFileName); // ex

            Console.Write(allText);

            Console.WriteLine("Press any key");
            Console.ReadKey();
        }
    }
}
