﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day07SimpleDatabase
{
    class Program
    {
        const string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\heok\School\dotnet\Day07SimpleDatabase\SimpleDB.mdf;Integrated Security=True;Connect Timeout=30";
        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();

            SqlCommand cmdInsert = new SqlCommand("INSERT INTO People (Name,Age) VALUES(@Name,@Age)",conn);
            cmdInsert.Parameters.AddWithValue("@Name", "Terry");
            cmdInsert.Parameters.AddWithValue("@Age", 22);
            cmdInsert.ExecuteNonQuery();

            SqlCommand cmdSelectAll = new SqlCommand("SELECT * FROM People", conn);
            SqlDataReader sqlReader = cmdSelectAll.ExecuteReader();
            while (sqlReader.Read())
            {
                int id = sqlReader.GetInt32(sqlReader.GetOrdinal("Id"));
                string name = sqlReader.GetString(sqlReader.GetOrdinal("Name"));
                int age = sqlReader.GetInt32(sqlReader.GetOrdinal("Age"));
                Console.WriteLine("#{0}: {1} is {2} y/o", id, name, age);
            }

            Console.Write("Press any key...");
            Console.Read();
        }
    }
}
