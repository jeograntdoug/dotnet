﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day03TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            CaculateOutput();
        }

       
        private void RbInputCel_Checked(object sender, RoutedEventArgs e)
        {
            CaculateOutput();
        }

        private void RbInputFah_Checked(object sender, RoutedEventArgs e)
        {
            CaculateOutput();
        }

        private void RbInputKel_Checked(object sender, RoutedEventArgs e)
        {
            CaculateOutput();
        }

        private void RbOutputCel_Checked(object sender, RoutedEventArgs e)
        {
            CaculateOutput();
        }

        private void RbOutputFah_Checked(object sender, RoutedEventArgs e)
        {
            CaculateOutput();
        }

        private void RbOutputKel_Checked(object sender, RoutedEventArgs e)
        {
            CaculateOutput();
        }

        private void CaculateOutput()
        {
            //try
            //{

                string inputTempStr = tbInput.Text;
                double inputTemp = double.Parse(inputTempStr);
                double inputTempC;

                // Convert all temp to celcius
                if (rbInputCel.IsChecked == true)
                {
                    inputTempC = inputTemp;
                }
                else if (rbInputFah.IsChecked == true)
                {
                    inputTempC = (inputTemp - 32) * 5 / 9;
                }
                else if (rbInputKel.IsChecked == true)
                {
                    inputTempC = inputTemp - 273.15;
                }
                else
                {
                    MessageBox.Show(this, "Error in input radio button", "Internel Error");
                    return;
                }

                double outputTemp;
                // Convert celcius temp to selected temp type
                if (rbOutputCel.IsChecked == true)
                {
                    outputTemp = inputTempC;
                }
                else if (rbOutputFah.IsChecked == true)
                {
                    outputTemp = (inputTempC * 5 / 9) + 32;
                }
                else if (rbOutputKel.IsChecked == true)
                {
                    outputTemp = inputTempC + 273.15;
                }
                else
                {
                    MessageBox.Show(this, "Error in input radio button", "Internel Error");
                    return;
                }

                lblOutput.Content = string.Format("{0:0.00}",outputTemp);

            //}
            //catch (FormatException ex)
            //{
            //    Console.WriteLine("Invalid Temp value");
            //}
        }

    }


}
