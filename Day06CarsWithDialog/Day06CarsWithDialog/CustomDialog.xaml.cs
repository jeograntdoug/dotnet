﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Day06CarsWithDialog
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class CustomDialog : Window
    {
        Car currentCar;
        Car newCar;
        public CustomDialog(Car car)
        {
            currentCar = car;
            InitializeComponent();

            comboFuelType.ItemsSource = Enum.GetValues(typeof(Car.FuelTypeEnum));

            updateFields();
        }

        private void updateFields()
        {
            if (currentCar == null) {
                lblIdVal.Content = "-";
                tbMakeModel.Text = "";
                sliderEngineSize.Value = 1.9;
                comboFuelType.SelectedIndex = 0;
                btSave.Content = "Add";
                return;
            }

            tbMakeModel.Text = currentCar.MakeModel;
            sliderEngineSize.Value = currentCar.EngineSizeL;
            comboFuelType.SelectedItem = currentCar.FuelType;
            btSave.Content = "Update";
            return;
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                newCar = new Car(
                        tbMakeModel.Text,
                        sliderEngineSize.Value,
                        (Car.FuelTypeEnum)comboFuelType.SelectedItem
                    );
            } catch (ArgumentException ex)
            {
                MessageBox.Show(this, "Error: " + ex.Message, "Validation Error");
                return;
            }
            this.DialogResult = true;
        }

        public Car NewCar { get { return newCar; } }
    }
}
