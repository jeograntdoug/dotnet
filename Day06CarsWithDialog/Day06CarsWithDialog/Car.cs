﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day06CarsWithDialog
{
    public class Car
    {
        public Car(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if(data.Length != 3)
            {
                throw new ArgumentException("Invalid Data Line: " + dataLine);
            }
            MakeModel = data[0];
            SetEngineSizeL(data[1]);
            SetFuelType(data[2]);
        }
        public Car(string makeModel,double engineSizeL,FuelTypeEnum fuelType)
        {
            MakeModel = makeModel;
            EngineSizeL = engineSizeL;
            FuelType = fuelType;
        }

        public enum FuelTypeEnum { Gasoline, Diesel, Hybrid, Electric, Other }
        string _makeModel; // 2-50 characters, no semicolons
        double _engineSizeL; // 0-20
        public FuelTypeEnum FuelType { get; set; }
        public string MakeModel
        {
            get { return _makeModel; }
            set
            {
                if (!Regex.IsMatch(value, "[^;]{2,50}"))
                {
                    throw new ArgumentException("Make Model Must be 2-50 characters, No semicolons.");
                }
                _makeModel = value;
            }
        }   

        public double EngineSizeL
        {
            get { return _engineSizeL; }
            set
            {
                if(value < 0 || value > 20)
                {
                    throw new ArgumentException("Engine Size Must be 0-20 L.");
                }
                _engineSizeL = value;
            }
        }

        private void SetEngineSizeL(string engineSizeLStr)
        {
            double engineSizeL;
            if(!double.TryParse(engineSizeLStr, out engineSizeL))
            {
                throw new ArgumentException("Engine size Must be 0-20 L.");
            }
            EngineSizeL = engineSizeL;
        }
        
        private void SetFuelType(string fuelType)
        {
            FuelTypeEnum fType;
            if(Enum.TryParse<FuelTypeEnum>(fuelType, out fType))
            {
                if (fType.ToString().Equals(fuelType))
                {
                    FuelType = fType;
                    return;
                }
            }
            throw new ArgumentException("FuelType has to be Gasoline, Diesel, Hybrid, Electric or Other.");
        }

        public string ToDataString()
        {
            return string.Format("{0};{1:0.0};{2}", MakeModel, EngineSizeL, FuelType);
        }
        public override string ToString()
        {
            return string.Format("{0}, {1}L engine, {2} fuel",MakeModel,EngineSizeL,FuelType);
        }
    }
}
