﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CsvHelper;
using Microsoft.Win32;

namespace Day06CarsWithDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Car> carList = new List<Car>();
        const string filePath = @"..\..\carstxt";
        public MainWindow()
        {
            InitializeComponent();
            LoadDataFromFile();
            lvCars.ItemsSource = carList;
        }


        private void LoadDataFromFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    
                    while (!sr.EndOfStream)
                    {
                        try
                        {
                            string dataLine = sr.ReadLine();
                            carList.Add(new Car(dataLine));
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                    txtStatus.Text = carList.Count + " Cars Loaded";
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(
                    this,
                    "Cannot open file: " + filePath,
                    "Loading File Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void SaveDataToFile(string fPath)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(File.Create(fPath)))
                {
                    foreach (Car c in carList)
                    {
                        sw.WriteLine(c.ToDataString());
                    }
                }
            }
            catch (IOException ex)
            {
                MessageBox.Show(
                    this,
                    "Cannot save Data",
                    "Saving data Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void EditCar()
        {
            Car currentCar = (Car)lvCars.SelectedItem;
            if (currentCar == null)
            {
                return;
            }

            CustomDialog dlg = new CustomDialog(currentCar);

            if (dlg.ShowDialog() == true)
            {
                string statusStr = "Editted: \"" + currentCar + "\"=> ";
                currentCar.MakeModel = dlg.NewCar.MakeModel;
                currentCar.EngineSizeL = dlg.NewCar.EngineSizeL;
                currentCar.FuelType = dlg.NewCar.FuelType;

                lvCars.Items.Refresh();
                txtStatus.Text = statusStr + dlg.NewCar;
            }
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveDataToFile(filePath);
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            SaveDataToFile(filePath);
            Close();
        }

        private void miAdd_Click(object sender, RoutedEventArgs e)
        {
            CustomDialog dlg = new CustomDialog(null);
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {
                carList.Add(dlg.NewCar);
                txtStatus.Text = "New Car Added: " + dlg.NewCar.ToString();
                lvCars.Items.Refresh();
            }
        }

        private void miExport_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            if (saveDialog.ShowDialog() == true)
            {
                string fName = saveDialog.FileName;
                if(!Regex.IsMatch(fName, @".+\.csv", RegexOptions.IgnoreCase))
                {
                    fName += ".csv";
                }
                //SaveDataToFile(fName);
                ExportDataToCSVFile(fName);
                txtStatus.Text = "Data Exported: " + fName;
            }
        }

        void ExportDataToCSVFile(string fileName)
        {
            try
            {
                using(StreamWriter sw = new StreamWriter(fileName))
                using(CsvWriter cw = new CsvWriter(sw, CultureInfo.InvariantCulture))
                {
                    cw.WriteRecords(carList); 
                }
            }catch (IOException ex)
            {
                MessageBox.Show(
                    this,
                    "Cannot save data",
                    "Saving data Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
        private void lvCars_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditCar();
        }
        private void lvCars_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtStatus.Text = "You have " + carList.Count + " car(s) currently";
        }

        private void lvCars_MouseRightButtonClick(object sender, MouseButtonEventArgs e)
        {
            if(lvCars.SelectedIndex == -1) { return;  }

            ContextMenu cm = this.FindResource("rClickMenu") as ContextMenu;
            cm.PlacementTarget = sender as ListView;
            cm.IsOpen = true;
        }

        private void rClickMenu_miEdit_Click(object sender, RoutedEventArgs e)
        {
            int selIndex = lvCars.SelectedIndex;
            if (selIndex == -1)
            {
                MessageBox.Show(this,
                    "This shouldn't happen",
                    "Internal Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            EditCar();
        }

        private void rClickMenu_miDelete_Click(object sender, RoutedEventArgs e)
        {
            int selIndex = lvCars.SelectedIndex;
            if(selIndex == -1)
            {
                MessageBox.Show(this,
                    "This shouldn't happen",
                    "Internal Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            carList.RemoveAt(selIndex);
            lvCars.Items.Refresh();
        }
    }
}
