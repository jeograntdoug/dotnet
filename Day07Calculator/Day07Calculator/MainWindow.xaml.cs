﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        const string fileName = @"..\..\resord.txt";
        enum Stage { FirstInput,SecondInput,Result }
        List<Button> numBtnList, operatorBtnList, resultBtnList;

        Stage crtStage = Stage.FirstInput;
        double firstValue, secondValue;

        string prevValue = ""; // For Copy paste event handling
        public MainWindow()
        {
            InitializeComponent();

            // Make Button List
            List<Button> btnList = new List<Button>();
            foreach(Button btn in gridButtons.Children)
            {
                btnList.Add(btn);
            }

            // 0~9,.,+/- button list
            numBtnList = new List<Button>(
                from btn in btnList
                where Regex.IsMatch(btn.Content.ToString(), "[0-9.]|(\\+\\/-)")
                select btn
                );
            // +, -, *, / button list
            operatorBtnList = new List<Button>(
                from btn in btnList
                where Regex.IsMatch(btn.Content.ToString(), "^[+*/-]$")
                select btn
                );
            // = button list ( for consistency )
            resultBtnList = new List<Button>();
            resultBtnList.Add(btEqual);
        }

        private void resetScreen()
        {
            switch (crtStage)
            {
                case Stage.FirstInput:
                    enableBtns(numBtnList, true);
                    enableBtns(operatorBtnList, true);
                    enableBtns(resultBtnList, false);

                    tbFirstVal.IsEnabled = true;
                    Keyboard.Focus(tbFirstVal);

                    tbFirstVal.Text = "";
                    tbSecondVal.Text = "";
                    tbSecondVal.IsEnabled = false;
                    lblOperation.Content = "?";
                    txtResult.Text = "";
                    break;
                case Stage.SecondInput:
                    enableBtns(numBtnList, true);
                    enableBtns(operatorBtnList, false);
                    enableBtns(resultBtnList, true);

                    tbFirstVal.IsEnabled = false;
                    tbSecondVal.IsEnabled = true;
                    Keyboard.Focus(tbSecondVal);
                    break;
                case Stage.Result:
                    enableBtns(numBtnList, false);
                    enableBtns(operatorBtnList, false);
                    enableBtns(resultBtnList, false);

                    tbFirstVal.IsEnabled = false;
                    tbSecondVal.IsEnabled = false;
                    btClear.IsEnabled = true;
                    break;
                default:
                    MessageBox.Show(this,
                        "Internal Error",
                        "Internal Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
            }
            prevValue = ""; // Every stage change, previous Value of input sets empty;
        }

        private void enableBtns(List<Button> list,bool isEnable)
        {
            foreach(Button btn in list)
            {
                btn.IsEnabled = isEnable;
            }
        }

        private bool IsValidInput(string valueStr)
        {
            if (valueStr == null) { return false; }
            if (!Regex.IsMatch(valueStr, "^[-]{0,1}[0-9]+\\.{0,1}[0-9]*$")) { return false; }
            return true;
        }

        private double parseInput(string valueStr)
        {
            double value;
            bool isNegative = false;
            bool hasPointAtTheEnd = false;

            isNegative = valueStr[0] == '-';
            hasPointAtTheEnd = valueStr[valueStr.Length - 1] == '.';

            if (isNegative)
            {
                valueStr = valueStr.Substring(1);
            }
            if (hasPointAtTheEnd)
            {
                valueStr = valueStr.Substring(0, valueStr.Length - 1);
            }

            if (!double.TryParse(valueStr, out value))
            {
                throw new ArgumentException("Invalid Input: " + valueStr);
            }

            if (isNegative)
            {
                return value * (-1);
            }
            return value;
        }


        // Evants
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            resetScreen();
            Keyboard.Focus(tbFirstVal);
        }

        private void tbNumber_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            string text = btn.Content.ToString();
            TextBox focusedTextBox
                = crtStage == Stage.FirstInput? tbFirstVal : tbSecondVal;
            switch (text)
            {
                case "+/-":
                    if(focusedTextBox.Text.Length == 0) { return; }
                    focusedTextBox.Text =
                        focusedTextBox.Text[0] == '-'? 
                        focusedTextBox.Text.Substring(1) : "-" + focusedTextBox.Text;
                    break;
                case ".":
                    if (focusedTextBox.Text.Length == 0) { return; }
                    if (Regex.IsMatch(focusedTextBox.Text, "\\."))
                    {
                        MessageBox.Show(this,
                            "Cannot have multiple <.>",
                            "Invalid Number",
                            MessageBoxButton.OK,
                            MessageBoxImage.Error);
                        return;
                    }

                    focusedTextBox.Text += text;
                    break;
                case "0": case "1": case "2": case "3": case "4":
                case "5": case "6": case "7": case "8": case "9":
                    focusedTextBox.Text += text;
                    break;
                default:
                    MessageBox.Show(this,
                        "It souldn't happen",
                        "Internal Error",
                        MessageBoxButton.OK,
                        MessageBoxImage.Error);
                    return;
            }

        }

        private void tbFirstSecondVal_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textBox = (TextBox)sender;
            if (IsValidInput(textBox.Text + e.Text)) {
                prevValue = textBox.Text + e.Text;
                e.Handled = false; 
            }
            else { 
                Console.WriteLine("It's not valid: " + e.Text);
                e.Handled = true;
            }
            
        }

        // Copy/paste Event handling
        /**
         * Another Method: https://stackoverflow.com/questions/3061475/paste-event-in-a-wpf-textbox
         * 
         * For backspace, please check the PreviewKeyDown event

            For paste command, add a command binding to the ApplicationCommands.Paste, and set the argument to handled, if you do not wish to do anything with it:

            <Window.CommandBindings>
                      <CommandBinding Command="ApplicationCommands.Paste"
                                      Executed="PasteExecuted" />
            </Window.CommandBindings>
            And in code behind:

            private void PasteExecuted(object sender, ExecutedRoutedEventArgs e)
            {
                        e.Handled = true;
            }
         * 
         * For clip board value: Clipboard.GetText(); 
         */
        private void tbFirstSecondVal_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox crtTextBox = (TextBox)sender;
            if (crtTextBox.Text == "") { return; }
            if (crtTextBox.Text.Equals(prevValue)) { return; }// Already took care in the "tbFirstSecondVal_PreviewTextInput" Method

            if (!IsValidInput(crtTextBox.Text))
            {
                MessageBox.Show(this,
                    "Invalid Input: " + crtTextBox.Text,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                crtTextBox.Text = prevValue;
                return;
            }
            prevValue = crtTextBox.Text;
        }

        private void btClear_Click(object sender, RoutedEventArgs e)
        {
            crtStage = Stage.FirstInput;
            resetScreen();
        }

        private void btOperation_Click(object sender, RoutedEventArgs e)
        {
            if (!IsValidInput(tbFirstVal.Text))
            {
                MessageBox.Show(this,
                    "Invalid Input: " + tbFirstVal.Text,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                tbFirstVal.Text = "";
                Keyboard.Focus(tbFirstVal);
                return;
            }

            try { 
                firstValue = parseInput(tbFirstVal.Text);

                Button clickedBtn = (Button)sender;
                lblOperation.Content = clickedBtn.Content;
                crtStage = Stage.SecondInput;
                resetScreen();
            }
            catch ( ArgumentException ex)
            {
                MessageBox.Show(this,
                    "Invalid Input: " + ex.Message,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                tbFirstVal.Text = "";
            }
        }

        private void tbResult_Click(object sender, RoutedEventArgs e)
        {
            if (!IsValidInput(tbSecondVal.Text))
            {
                MessageBox.Show(this,
                    "Invalid Input: " + tbSecondVal.Text,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                tbSecondVal.Text = "";
                Keyboard.Focus(tbSecondVal);
                return;
            }

            try
            {
                secondValue = parseInput(tbSecondVal.Text);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    "Invalid Input: " + ex.Message,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                tbSecondVal.Text = "";
                return;
            }

            switch (lblOperation.Content)
            {
                case "+":
                    txtResult.Text = string.Format("{0:G}",firstValue + secondValue);
                    break;
                case "-":
                    txtResult.Text = string.Format("{0:G}", firstValue - secondValue);
                    break;
                case "/":
                    txtResult.Text = string.Format("{0:G}", firstValue / secondValue);
                    break;
                case "*":
                    txtResult.Text = string.Format("{0:G}", firstValue * secondValue);
                    break;
                default:
                    MessageBox.Show(this,
                    "This shouldn't happen",
                    "Internal Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                    return;
            }

            crtStage = Stage.Result;
            resetScreen();
        }
    }
}
