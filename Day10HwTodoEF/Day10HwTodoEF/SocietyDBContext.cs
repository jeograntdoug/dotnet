﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10HwTodoEF
{
    class SocietyDBContext : DbContext
    {
        virtual public DbSet<Todo> Todos { get; set; }
    }
}
