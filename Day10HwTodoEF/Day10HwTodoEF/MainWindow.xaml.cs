﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10HwTodoEF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SocietyDBContext ctx;

        public MainWindow()
        {
            InitializeComponent();

            ctx = new SocietyDBContext();

            lvTasks.ItemsSource = (from todo in ctx.Todos select todo).ToList<Todo>();
            comboStatus.ItemsSource = Enum.GetValues(typeof(Todo.StatusEnum));

            btDelete.IsEnabled = false;
            btUpdate.IsEnabled = false;
        }

        private void lvTasks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Todo selTodo = (Todo)lvTasks.SelectedItem;
            if(selTodo == null)
            {
                tbTask.Text = "";
                sliderDifficulty.Value = 2;
                dpDueDate.SelectedDate = null;
                comboStatus.SelectedIndex = 0;
                btDelete.IsEnabled = false;
                btUpdate.IsEnabled = false;
                return;
            }

            tbTask.Text = selTodo.Task;
            sliderDifficulty.Value = selTodo.Difficulty;
            dpDueDate.SelectedDate = selTodo.DueDate;
            comboStatus.SelectedItem = selTodo.Status;
            btDelete.IsEnabled = true;
            btUpdate.IsEnabled = true;
        }

        private void btAdd_Click(object sender, RoutedEventArgs e)
        {
            try { 
                ctx.Todos.Add(new Todo()
                {
                    Task = tbTask.Text,
                    Difficulty = (int)sliderDifficulty.Value,
                    DueDate = dpDueDate.SelectedDate,
                    Status = (Todo.StatusEnum)comboStatus.SelectedItem
                }) ;
                ctx.SaveChanges();
                lvTasks.ItemsSource = (from todo in ctx.Todos select todo).ToList<Todo>();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    ex.Message,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }

        private void btDelete_Click(object sender, RoutedEventArgs e)
        {
            Todo selTodo = (Todo)lvTasks.SelectedItem;
            if(selTodo == null)
            {
                MessageBox.Show(this,
                    "Select Item on the list.",
                    "Delete Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            ctx.Todos.Remove(selTodo);
            ctx.SaveChanges();
            lvTasks.ItemsSource = (from todo in ctx.Todos select todo).ToList<Todo>();
        }

        private void btUpdate_Click(object sender, RoutedEventArgs e)
        {
            Todo selTodo = (Todo)lvTasks.SelectedItem;
            if (selTodo == null)
            {
                MessageBox.Show(this,
                    "Select Item on the list.",
                    "Delete Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return;
            }

            try { 
                selTodo.Task = tbTask.Text;
                selTodo.Difficulty = (int)sliderDifficulty.Value;
                selTodo.DueDate = dpDueDate.SelectedDate;
                selTodo.Status = (Todo.StatusEnum)comboStatus.SelectedItem;

                ctx.SaveChanges();
                lvTasks.ItemsSource = (from todo in ctx.Todos select todo).ToList<Todo>();
            } catch (ArgumentException ex)
            {
                MessageBox.Show(this,
                    ex.Message,
                    "Validation Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
    }
}
