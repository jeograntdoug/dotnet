﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Day10HwTodoEF
{
    class Todo
    {
        public enum StatusEnum { Pending=1, Done=2}
        
        public int Id { get; set; }

        private string _task;

        private int _difficulty;

        private DateTime _dueDate;
        [Required]
        public DateTime? DueDate
        {
            get { return _dueDate; }
            set
            {
                if(value == null)
                {
                    throw new ArgumentException("Pick due Date.");
                }
                _dueDate = value.Value;
            }
        }

        [Required]
        [EnumDataType(typeof(StatusEnum))]
        public StatusEnum Status { get; set; }

        [Required]
        [StringLength(100)]
        public string Task
        {
            get { return _task; }
            set
            {
                if(value == null)
                {
                    throw new ArgumentException("Task must be 1~100 characters");
                }
                if (value.Length > 100 || value.Length < 1)
                {
                    throw new ArgumentException("Task must be 1~100 characters");
                }

                _task = value;
            }
        }

        [Required]
        public int Difficulty
        {
            get { return _difficulty; }
            set
            {
                if(value > 6 || value < 0)
                {
                    throw new ArgumentException("Difficulty must be 1~5");
                }
                _difficulty = value;
            }
        }

    }
}
