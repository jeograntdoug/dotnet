﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01Cities
{
    class City
    {   
        public string Name;
        public double PopulationMillions;
        public override string ToString()
        {
            return string.Format("City: {0}, with {1} mils", Name, PopulationMillions);
        }
    }

    class BetterCity
    {
        public BetterCity (string name)
        {
            Name2 = name;
        }

        private string name { get; set; }

        private string _name2;
        public string Name2
        {
            get
            {
                return _name2;
            }
            set
            {
                if(value.Length < 2)
                {
                    throw new InvalidOperationException();
                }
                this._name2 = value;
            }
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            City newCity = new City { Name = "Montreal", PopulationMillions = 3.5 };
            List<City> cityList = new List<City>();
            cityList.Add(newCity);
            Console.WriteLine("new City is {0}", newCity);

            BetterCity bc1 = new BetterCity("Toronto");
            bc1.Name2 = "Montreal";
            //bc1.Name2 = "a";
            //bc1._name2 = "Toronto";
            //
            Console.Write("Press Any Key to finish...");
            Console.ReadKey();
        }
    }
}
