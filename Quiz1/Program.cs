﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1
{
    class Program
    {
        static List<Airport> airportList = new List<Airport>();
        const string FilePath = @"..\..\data.txt";
        const string LogFIlePath = @"..\..\events.log";

        static void Main(string[] args)
        {
            LoadDataFromFile();
            while (true)
            {
                PrintMenu();
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        AddAirport();
                        break;
                    case "2":
                        PrintAllAirportList();
                        break;
                    case "3":
                        NearestAirport();
                        break;
                    case "4":
                        ElevationStandardDeviation();
                        break;
                    case "5":
                        ChangeLogs();
                        break;
                    case "0":
                        SaveDataToFile();
                        Console.WriteLine("Good bye.");
                        Console.WriteLine("Press any key...");
                        Console.ReadKey();
                        return;
                    default:
                        Console.WriteLine("Invalid menu try again.");
                        break;
                }
                Console.WriteLine();
            }
        }

        static void AddAirport() {
            Console.WriteLine("Adding Airport");
            Console.Write("Enter Code: ");
            string code = Console.ReadLine();

            Console.Write("Enter City: ");
            string city = Console.ReadLine();

            Console.Write("Enter Latitude: ");
            string latitude = Console.ReadLine();

            Console.Write("Enter Longitude: ");
            string longitude = Console.ReadLine();

            Console.Write("Enter Elevation in meters: ");
            string elevM = Console.ReadLine();

            try
            {
                airportList.Add(new Airport(string.Join(";", code, city, latitude, longitude, elevM)));
                Console.WriteLine("Airport Added.");
            } catch (InvalidDataException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        static void PrintAllAirportList() {
            if(airportList.Count == 0)
            {
                Console.WriteLine("There is no airport in the list.");
                return;
            }

            int count = 0;
            foreach(Airport a in airportList)
            {
                Console.WriteLine(count++ + "#: " + a);
            }
        }

        static void NearestAirport() {
            if (airportList.Count < 2)
            {
                Console.WriteLine("Not enougn airports in the list. Must be 2 or more in the list");
                return;
            }

            PrintAllAirportList();

            try {
                Console.Write("Please Enter Airport Code(From): ");
                string fromStr = Console.ReadLine();

                var fromAirportList = (from a in airportList where a.Code == fromStr select a);
                if(fromAirportList.Count() == 0)
                {
                    Console.WriteLine("There is no {0} airport code in the list", fromStr);
                    return;
                }
                Airport fromAirport = fromAirportList.First();

                GeoCoordinate gc = new GeoCoordinate(fromAirport.Latitude, fromAirport.Longitude);

                var closestAirport = 
                    from a in airportList
                    where a.Code != fromAirport.Code
                    orderby gc.GetDistanceTo(
                        new GeoCoordinate(a.Latitude,a.Longitude)
                        ) ascending
                    select a;
                Console.WriteLine(closestAirport.First());
            } catch (FormatException ex)
            {
                Console.WriteLine("Invalid Airport Number");
            }
        }

        static void ElevationStandardDeviation() {
            if (airportList.Count < 2)
            {
                Console.WriteLine("Not enougn airports in the list. Must be 2 or more in the list");
                return;
            }

            var average = (from a in airportList select a.ElevationMeters).Average();
            var standardDeviation 
                = Math.Sqrt((from a in airportList
                     select Math.Pow((a.ElevationMeters - average),2))
                     .Sum() / (airportList.Count -1)
                    );
            Console.WriteLine("For all airports the standard deviation of their elevation is {0:0.00}", standardDeviation);
        }

        static void ChangeLogs() {
            Console.Write(
@"Changing logging settings:
1-Logging to console
2-Logging to file
Enter your choices, comma-separated, empty for none:"
            );
            string choice = Console.ReadLine();
            
            switch (choice)
            {
                case "1":
                    Airport.Logger = LogToConsole;
                    Console.WriteLine("Logging to console enabled");
                    break;
                case "2":
                    Airport.Logger = LogToFile;
                    Console.WriteLine("Logging to file enabled.");
                    break;
                case "1,2":
                    Airport.Logger = LogToConsole;
                    Console.WriteLine("Logging to console enabled");
                    Airport.Logger += LogToFile;
                    Console.WriteLine("Logging to file enabled.");
                    break;
                default:
                    Console.WriteLine("Invalid setting. Go to main menu");
                    break;
            }
        }

        public static void LogToConsole(string msg) {
            Console.WriteLine(msg);
        }

        public static void LogToFile(string msg) {
            try
            {
                File.AppendAllText(LogFIlePath, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ") + msg + "\n");
            } catch(IOException ex)
            {
                Console.WriteLine("Cannot write log in the file.");
            }
        }

        static void PrintMenu()
        {
            Console.WriteLine();
            Console.Write(
@"Menu:
1. Add Airport
2. List all airports
3. Find nearest airport by code (preferably using LINQ)
4. Find airport's elevation standard deviation (using LINQ)
5. Change log delegates
0. Exit
Enter your choice:"
            );
        }

        static void SaveDataToFile() {
            try
            {
                var data = from a in airportList select a.ToDataString();
                File.WriteAllLines(FilePath,data);
                Console.WriteLine("Data svaed back to file.");
            }
            catch (IOException ex)
            {
                Console.WriteLine("Cannot save data");
            }
        }

        static void LoadDataFromFile() {
            try
            {
                string[] data = File.ReadAllLines(FilePath);
                foreach(string dataLine in data)
                {
                    try {
                        airportList.Add(new Airport(dataLine));
                    } catch (InvalidDataException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

            }
            catch (IOException ex)
            {
                Console.WriteLine("Cannot open file. continue...");
            }
        }
    }
}
