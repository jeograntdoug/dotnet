﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz1
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg) { }
    }

    public class Airport
    {
        public Airport(string code, string city, double lat, double lng, int elevM) {
            Logger?.Invoke("Constructor called");

            Code = code;
            City = city;
            Latitude = lat;
            Longitude = lng;
            ElevationMeters = elevM;
        }

        public Airport(string dataLine) {
            Logger?.Invoke("Constructor called");
            if (dataLine == null)
            {
                Logger?.Invoke("Invalid Data: null");
                throw new InvalidDataException("Invalid Data: null");
            }

            string[] data = dataLine.Split(';');
            if(data.Length != 5)
            {
                Logger?.Invoke("Invalid Data format: " + dataLine);
                throw new InvalidDataException("Invalid Data format: " + dataLine);
            }


            try
            {
                Code = data[0];
                City = data[1];
                Latitude = double.Parse(data[2]);
                Longitude = double.Parse(data[3]);
                ElevationMeters = int.Parse(data[4]);
            }
            catch (FormatException ex)
            {
                Logger?.Invoke("Invalid data format: " + dataLine);
                throw new InvalidDataException("Invalid data format: " + dataLine);
            }
        }

        public delegate void LoggerDelegate(string msg);
        public static LoggerDelegate Logger;

        string _code; // exactly 3 uppercase letters, use regexp
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (!Regex.IsMatch(value, "[A-Z]{3}"))
                {
                    Logger?.Invoke("Airport Code must be 3 uppercase letters");
                    throw new InvalidDataException("Airport Code must be 3 uppercase letters");
                }
                _code = value;
            }
        }

        string _city; // 1-50 characters, made up of uppercase and lowercase letters, digits, and .,- characters
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (!Regex.IsMatch(value, "[a-zA-Z0-9.,-]{1,50}"))
                {
                    Logger?.Invoke("City name must be 1~50 characters(letters,digits and .,-)");
                    throw new InvalidDataException("City name must be 1~50 characters(letters,digits and .,-)");
                }
                _city = value;
            }
        }

        double _latitude, _longitude; // -90 to 90, -180 to 180
        public double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                if(value < -90 || value > 90)
                {
                    Logger?.Invoke("Latitude must be -90 to 90");
                    throw new InvalidDataException("Latitude must be -90 to 90");
                }
                _latitude = value;
            }
        }

        public double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                if (value < -180 || value > 180)
                {
                    Logger?.Invoke("Longitude must be -180 to 180");
                    throw new InvalidDataException("Longitude must be -180 to 180");
                }
                _longitude = value;
            }
        }

        int _elevationMeters; //-1000 to 10000
        public int ElevationMeters
        {
            get
            {
                return _elevationMeters;
            }
            set
            {
                if(value < -1000 || value > 1000)
                {
                    Logger?.Invoke("ElevationMeters must be -1000 to 1000");
                    throw new InvalidDataException("ElevationMeters must be -1000 to 1000");
                }
                _elevationMeters = value;
            }
        }

	    public override string ToString() {
            return string.Format(
                "{0} in {1} at {2} lat / {3} lng at {4}m elevation",
                Code,City,Latitude,Longitude,ElevationMeters
                );
        }

        public string ToDataString() {
            return string.Format("{0};{1};{2};{3};{4}", 
                Code, City, Latitude, Longitude, ElevationMeters);
        }
    }
}
