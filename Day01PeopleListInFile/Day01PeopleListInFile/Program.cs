﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01PeopleListInFile
{
    public class Person
    {
        public Person(string name, int age, string city)
        {
            Name = name;
            Age = age;
            City = city;
        }
        public Person(string dataLine)
        {
            string[] data = dataLine.Split(';');

            if (data.Length != 3)
            {
                throw new ArgumentException("Invalid data line: " + dataLine);
            }

            try {
                Name = data[0];
                Age = int.Parse(data[1]);
                City = data[2];
            } catch (FormatException ex)
            {
                throw new ArgumentException("Invalid data line: " + dataLine);
            }
        }
        private string _name; // Name 2-100 characters long, not containing semicolons
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Contains(";") || value.Length > 100 || value.Length < 2)
                {
                    throw new ArgumentException("Name must be 2-100 characters long, not containing semicolons");
                }
                _name = value;
            }
        }

        private int _age; // Age 0-150
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentException("Age must be 0~150 range");
                }
                _age = value;
            }
        }

        private string _city; // City 2-100 characters long, not containing semicolons
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Contains(";") || value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentException("City must be 2-100 characters long, not containing semicolons");
                }
                _city = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0} is {1} from {2}", Name, Age, City);
        }
        public string ToDataString()
        {
            return string.Format("{0};{1};{2}", Name, Age, City);
        }
    }

    class Program
    {
        static List<Person> peopleList = new List<Person>();
        const string FileName = @"..\..\people.txt";

        static void Main(string[] args)
        {
            ReadAllPeopleFromFile();
            while (true)
            {
                PrintMenu();

                Console.Write("Choice: ");
                string choiceStr = Console.ReadLine();
                int choice;

                if (!int.TryParse(choiceStr, out choice))
                {
                    Console.WriteLine("Invalid choice try again.");
                    continue;
                }

                switch (choice)
                {
                    case 1:
                        AddPersonInfo();
                        break;
                    case 2:
                        ListAllPersonsInfo();
                        break;
                    case 3:
                        FindPersonByName();
                        break;
                    case 4:
                        FindPersonYoungerThan();
                        break;
                    case 0:
                        SaveAllPeopleToFile();
                        Console.WriteLine("Data saved.");
                        Console.WriteLine("Good bye!");
                        return;
                    default:
                        Console.WriteLine("Invalid choice try again.");
                        break;
                }
            }

        }
        static void PrintMenu()
        {
            Console.WriteLine("You will then display a menu to the user:");
            Console.WriteLine("1. Add person info");
            Console.WriteLine("2. List persons info");
            Console.WriteLine("3. Find a person by name");
            Console.WriteLine("4. Find all persons younger than age");
            Console.WriteLine("0. Exit");
        }

        static void ReadAllPeopleFromFile()
        {
            if (!File.Exists(FileName)) { return; }

            try
            {
                string[] dataArray = File.ReadAllLines(FileName); //ex
                foreach (string dataLine in dataArray)
                {
                    try {
                        peopleList.Add(new Person(dataLine)); // ex
                    } catch (ArgumentException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }catch(IOException ex)
            {
                Console.WriteLine("Fetal Error: Cannot Open file. Program exiting");
                Environment.Exit(0);
            }
        }

        static void SaveAllPeopleToFile()
        {
            try
            {
                List<String> dataList = new List<String>();
                foreach (Person p in peopleList)
                {
                    dataList.Add(p.ToDataString());
                }
                File.WriteAllLines(FileName, dataList);
            } catch(IOException ex)
            {
                Console.WriteLine("Fetal Error: Cannot Open file. Program exiting");
                Environment.Exit(0);
            }
        }

        static void AddPersonInfo()
        {
            try
            {
                Console.WriteLine("Adding a person.");

                Console.Write("Enter name: ");
                string name = Console.ReadLine();

                Console.Write("Enter age: ");
                string ageStr = Console.ReadLine();
                int age;
                if(!int.TryParse(ageStr,out age))
                {
                    throw new ArgumentException("Age must be integer");
                }

                Console.Write("Enter city: ");
                string city = Console.ReadLine();

                peopleList.Add(new Person(name,age, city));
                Console.WriteLine("Person added");
            } catch( ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine();
        }

        static void ListAllPersonsInfo()
        {
            Console.WriteLine("Listing all persons");
            foreach (Person p in peopleList)
            {
                Console.WriteLine(p);
            }
            Console.WriteLine();
        }

        static void FindPersonByName()
        {
            Console.Write("Enter partial person name: ");
            string filter = Console.ReadLine();
            string result = "";

            foreach (Person p in peopleList)
            {
                if (p.Name.Contains(filter))
                {
                    result += p.ToString();
                    result += "\n";
                }
            }
            if (result != "")
            {
                Console.WriteLine("Matches found:");
                Console.WriteLine(result);
            }
            else
            {
                Console.WriteLine("No Matches found");
            }

            Console.WriteLine();
        }

        static void FindPersonYoungerThan()
        {
            Console.Write("Enter maximum age: ");
            string filterStr = Console.ReadLine();
            int filter;
            if(!int.TryParse(filterStr,out filter))
            {
                Console.WriteLine("Invalid Age");
                return;
            }

            string result = "";

            foreach (Person p in peopleList)
            {
                if (p.Age < filter)
                {
                    result += p.ToString();
                    result += "\n";
                }
            }

            if(result != "")
            {
                Console.WriteLine("Matches found");
                Console.Write(result);
            }
            else
            {
                Console.WriteLine("No Matches found:");
            }
            Console.WriteLine();
        }
    }
}
