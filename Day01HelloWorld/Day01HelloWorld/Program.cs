﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("What is your name? ");
            string name = Console.ReadLine();

            Console.Write("How old are you? ");
            string ageStr = Console.ReadLine();
            int age;

            if(!int.TryParse(ageStr, out age))
            {
                Console.WriteLine("Invalid Age");
            }
            else
            {
                Console.WriteLine("Hello {0}, {1} y/o, nice to meet you {0}.", name, age);
            }

            Console.WriteLine("Press Any Key to finish");
            Console.ReadKey();
        }
    }
}
